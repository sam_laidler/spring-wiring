package sam.laidler.springwiring;

import org.junit.Test;

import static org.mockito.Mockito.*;

public class CustomerTest {
	
	@Test
	public void testHiring() {
		Item mockItem = mock(Item.class);
		Customer customer = new Customer(mockItem);
		customer.hireItem();
		verify(mockItem).maintenance();
	}
}
