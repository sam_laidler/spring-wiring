package sam.laidler.springwiring;

public class VIPCustomer extends Customer implements CustomerInterface {
	public VIPCustomer(ItemInterface item) {
		super(item);
		id = "VIP";
	}
}
