package sam.laidler.springwiring;

public class Item implements ItemInterface {
	protected String id = "Generic";
	
	@Override
	public void maintenance() {
		System.out.println(id + " item maintenance");
	}

	@Override
	public String toString() {
		return id + " item";
	}
}
