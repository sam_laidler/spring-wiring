package sam.laidler.springwiring;

public class Customer implements CustomerInterface {
	private ItemInterface item;
	protected String id = "Generic";
	
	public Customer(ItemInterface item) {
		super();
		this.item = item;
	}

	@Override
	public void hireItem() {
		System.out.println(id + " customer hiring " + item);
		item.maintenance();
	}

	@Override
	public void payBill() {
		System.out.println(id + " customer paying bill");
	}
}
