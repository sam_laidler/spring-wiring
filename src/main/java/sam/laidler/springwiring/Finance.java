package sam.laidler.springwiring;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class Finance {
	@After("execution(* sam.laidler.springwiring.Customer.hiteItem(..))")
	private void sendReceipt() {
		System.out.println("Sending receipt");
	}
}
