package sam.laidler.springwiring;

public interface CustomerInterface {
	public void hireItem();
	public void payBill();
}
