package sam.laidler.springwiring;

public class NormalCustomer extends Customer implements CustomerInterface {
	public NormalCustomer(ItemInterface item) {
		super(item);
		id = "Normal";
	}
}
