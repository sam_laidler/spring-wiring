package sam.laidler.springwiring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

import sam.laidler.springwiring.*;

@SpringBootApplication
public class SpringWiringApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext appContext = SpringApplication.run(SpringWiringApplication.class, args);
		
		VIPCustomer vip = (VIPCustomer) appContext.getBean("vip");
		vip.hireItem();
		vip.payBill();
		
		NormalCustomer normal = (NormalCustomer) appContext.getBean("normal");
		normal.hireItem();
		normal.payBill();
	}

	@Bean
	public Customer normal() {
		return new NormalCustomer(new Item());
	}
	
	@Bean
	public Customer vip() {
		return new VIPCustomer(new LuxuryItem());
	}
	
	@Bean
	public Finance finance() {
		return new Finance();
	}
}
