With this code I am trying to demonstrate loose coupling through dependency injection.

In theory, I could have instantiated an Item object inside the Customer. The problem with doing this, however,
is that I would not have been able to use the customer class for any other kind of item. For the VIP class
I would have to write a new class so that it could handle a luxury item.

Testing, also, would have been an issue. I want to make sure that, every time the customer object makes a call to
hire an item, the item itself also makes a call to the maintenance method. If I had instantiated an Item object
directly inside the Customer, I could verify the behaviour, but only by calling the functional version of the code.
I do not want this. The functional version of the maintenance method would trigger operations I do not want when
testing. By using dependency injection, I can inject a mock object. This way I can ensure that the test version of 
the maintenance method is called, which is harmless. Notice, when the JUnit test is run, there is no message written
to screen. 

Finally, there is some aspect oriented programming. I wanted to send a receipt every time the customer object makes a
call to hire the item. I have treated this as a 'cross cutting' concern, as it cuts across the relevant customer method,
without actually being directly relevant to a customer.

Running the code the following is dumped to screen:

VIP customer hiring Luxury item
Luxury item maintenance
VIP customer paying bill
Normal customer hiring Generic item
Generic item maintenance
Normal customer paying bill

Runnning the Junit test (notice no item maintenance messages dumped to screen):

Generic customer hiring Mock for Item, hashCode: 848097505